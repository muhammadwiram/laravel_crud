@extends('layout/main')

@section('title', 'Edit Student')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-8">
          <h1 class="mt-3">Edit Student</h1>
          <form action="/students/{{ $student->id }}" method="post">
            @method("patch")
            @csrf
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control @error ('name') is-invalid @enderror" id="name" placeholder="Your name" name="name" value="{{ $student->name }}">
              @error('name')<div class="invalid-feedback">{{ $message }}</div>@enderror
            </div>
            <div class="form-group">
              <label for="nrp">NRP</label>
              <input type="text" class="form-control @error ('nrp') is-invalid @enderror" id="nrp" placeholder="Your nrp" name="nrp" value="{{ $student->nrp }}">
              @error('nrp')<div class="invalid-feedback">{{ $message }}</div>@enderror
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              <input type="text" class="form-control @error ('email') is-invalid @enderror" id="email" placeholder="Your email" name="email" value="{{ $student->email }}">
              @error('email')<div class="invalid-feedback">{{ $message }}</div>@enderror
            </div>
            <div class="form-group">
              <label for="major">Major</label>
              <input type="text" class="form-control @error ('major') is-invalid @enderror" id="major" placeholder="Your major" name="major" value="{{ $student->major }}">
              @error('major')<div class="invalid-feedback">{{ $message }}</div>@enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit Data</button>
          </form>
        </div>
    </div>
</div>
@endsection
