@extends('layout/main')

@section('title', 'Student List')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-5">
            <h1 class="mt-2">Student List</h1>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <a href="/students/create" class="btn btn-primary my-2">Add New Data</a>
            <ul class="list-group">
              @foreach($students as $student)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                  {{ $student->name }}
                  <a href="/students/{{ $student->id }}" class="badge badge-primary badge-pill">Detail</a>
                </li>
              @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection
