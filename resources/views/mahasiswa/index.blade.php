@extends('layout/main')

@section('title', 'Daftar Mahasiswa')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Daftar Mahasiswa</h1>
            <table class="table mt-3">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">NRP</th>
                  <th scope="col">Email</th>
                  <th scope="col">Major</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($mahasiswa as $m)
                <tr>
                  <th scope="row">{{ $loop->iteration }}</th>
                  <td>{{ $m->name }}</td>
                  <td>{{ $m->nrp }}</td>
                  <td>{{ $m->email }}</td>
                  <td>{{ $m->major }}</td>
                  <td>
                      <a href="#" class="badge badge-success">Edit</a>
                      <a href="#" class="badge badge-danger">Delete</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
