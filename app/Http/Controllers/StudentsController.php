<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        // ['students' => $students] bisa diganti dengan compact('students');
        return view('students/index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $student = new Student;
        // $student->name = $request->name;
        // $student->nrp = $request->nrp;
        // $student->email = $request->email;
        // $student->major = $request->major;

        // $student->save();

        // Student::create([
        //     'name' => $request->name,
        //     'nrp' => $request->nrp,
        //     'email' => $request->email,
        //     'major' => $request->major
        // ]);

        $request->validate([
            'name' => 'required',
            'nrp' => 'required|size:9',
            'email' => 'required',
            'major' => 'required'
        ]);

        Student::create($request->all());
        return redirect('/students')->with('status', 'Data added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('students/show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('students/edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $request->validate([
            'name' => 'required',
            'nrp' => 'required|size:9',
            'email' => 'required',
            'major' => 'required'
        ]);

        Student::where('id', $student->id)
                ->update([
                    'name' => $request->name,
                    'nrp' => $request->nrp,
                    'email' => $request->email,
                    'major' => $request->major
                ]);
        return redirect('/students')->with('status', 'Data updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        Student::destroy($student->id);
        return redirect('/students')->with('status', 'Data successfully deleted.');
    }
}
